/*
* This represents a board in the life world
* */
type Board = boolean[][];

/*
* Represents a handler for html
*
* */
interface DOMHandler {
    /* Query string for the given object */
    query(q: string): HTMLElement;

    /* Creates an object of the given type */
    create(name: string): HTMLElement;
}

export class HTMLDomHandler {
    query(q: string): HTMLElement {
        return document.querySelector(q);
    }

    create(name: string): HTMLElement {
        return document.createElement(name);
    }
}


export class Life {
    private DX = [-1, -1, -1, 0, 0, 1, 1, 1];
    private DY = [-1, 0, 1, -1, 1, -1, 0, 1];
    private world: Board;

    constructor(public length: number) {
        this.world = this.newWorld();
    }

    public tick() {
        const world = this.newWorld();

        for (let i = 0; i < this.length; i++) {
            for (let j = 0; j < this.length; j++) {
                world[i][j] = this.cellActivation(i, j);
            }
        }

        this.world = world;
    }

    public toggle(i: number, j: number) {
        this.world[i][j] = !this.world[i][j];
    }

    public get(i: number, j: number): boolean {
        return this.world[i][j];
    }

    private cellActivation(i: number, j: number) {
        const neighbors = this.neighborsAlive(i, j);
        /*
        * Any live cell with fewer than two live neighbors dies, as if by underpopulation.
        * Any live cell with two or three live neighbors lives on to the next generation.
        * Any live cell with more than three live neighbors dies, as if by overpopulation.
        * Any dead cell with exactly three live neighbors becomes a live cell, as if by reproduction.
        */

        if (this.world[i][j]) {
            return neighbors === 2 || neighbors === 3;
        } else {
            return neighbors === 3;
        }
    }

    private neighborsAlive(i: number, j: number): number {
        let neighbors = 0;
        for (let d = 0; d < this.DX.length; d++) {
            let nx = (i + this.DX[d]) % this.length;
            let ny = (j + this.DY[d]) % this.length;

            if (nx < 0) {
                nx = this.length - 1;
            }

            if (ny < 0) {
                ny = this.length - 1;
            }

            if (this.world[nx][ny]) {
                neighbors += 1;
            }
        }

        return neighbors;
    }

    private newWorld() {
        let board = [];
        for (let i = 0; i < this.length; i++) {
            board.push([]);
            for (let j = 0; j < this.length; j++) {
                board[i].push(false);
            }
        }
        return board;
    }
}

export class LifeRenderer {
    private htmlHandler: DOMHandler;
    private world: Life;
    private play: HTMLButtonElement;
    private stop: HTMLButtonElement;
    private rootElt: HTMLElement;
    private intervalId: number;

    constructor(htmlHandler: DOMHandler, world: Life) {
        const RENDER_IN = '.life';
        this.htmlHandler = htmlHandler;
        this.world = world;
        this.rootElt = this.htmlHandler.query(RENDER_IN);
        this.initControlButtons();
        this.createElements();
    }

    public go() {
        clearInterval(this.intervalId);
        this.intervalId = setInterval(() => {
            this.world.tick();
            this.render();
        }, 100);
    }

    public halt() {
        clearInterval(this.intervalId);
        this.intervalId = null;
    }

    public removeElements() {
        while (this.rootElt.hasChildNodes()) {
            this.rootElt.removeChild(this.rootElt.firstChild);
        }
    }

    private createElements() {
        for (let i = 0; i < this.world.length; i++) {
            for (let j = 0; j < this.world.length; j++) {
                const btn = this.htmlHandler.create("button");
                btn.setAttribute("id", `${i},${j}`);
                btn.classList.add("cell");

                btn.addEventListener("click", () => {
                    this.world.toggle(i, j);
                    toggleClass(btn, this.world.get(i, j));
                });

                this.rootElt.appendChild(btn);
            }
        }
    }

    private initControlButtons() {
        const l = this.world.length;
        this.rootElt.style.gridTemplateRows = `repeat(${l}, 1fr)`;
        this.rootElt.style.gridTemplateColumns = `repeat(${l}, 1fr)`;
        this.rootElt.style.maxWidth = (l).toString() + 'rem';

        const playQuery = "#play-btn";
        const stopQuery = "#stop-btn";
        this.play = <HTMLButtonElement>this.htmlHandler.query(playQuery);
        this.stop = <HTMLButtonElement>this.htmlHandler.query(stopQuery);
        this.intervalId = null;
        this.play.addEventListener("click", () => {
            this.go();
        });

        this.stop.addEventListener("click", () => {
            this.halt();
        });
    }

    private render() {
        for (let i = 0; i < this.world.length; i++) {
            for (let j = 0; j < this.world.length; j++) {
                const btn = document.getElementById(`${i},${j}`);
                toggleClass(btn, this.world.get(i, j));
            }
        }
    }

}

function toggleClass(btn: HTMLElement, alive: boolean): void {
    btn.classList.remove("alive");
    btn.classList.remove("dead");
    if (alive) {
        btn.classList.add("alive");
    } else {
        btn.classList.add("dead");
    }
}
