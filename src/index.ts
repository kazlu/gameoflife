import {HTMLDomHandler, Life, LifeRenderer} from "./life-cor";

(function () {
    const LENGTH = 10;

    const htmlHandler = new HTMLDomHandler();

    const RESTART = '#restart-btn';
    const restartBtn = htmlHandler.query(RESTART);

    let life = new Life(LENGTH);
    let lifeRenderer = new LifeRenderer(htmlHandler, life);

    restartBtn.addEventListener("click", () => {
        const lengthTxt = <HTMLInputElement>htmlHandler.query("#length-txt");
        const newLength = parseInt(lengthTxt.value);

        lifeRenderer.halt();
        lifeRenderer.removeElements();

        life = new Life(newLength);
        lifeRenderer = new LifeRenderer(htmlHandler, life);
    });
})();
